<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'PostController@index');

Route::post('/store', 'PostController@store')->name('store');

Route::post('/delete/{post}', 'PostController@delete')->name('delete');

Route::post('/edit/{post}', 'PostController@edit')->name('edit');

Route::post('/destroy/{post}', 'PostController@destroy')->name('destroy');

Route::post('/update/{post}', 'PostController@update')->name('update');

Auth::routes(['verify' => true]);

Route::post('/member-register', 'Auth\RegisterController@membershipRegisterForm')->name('member-register');

Route::get('/user-verified', function() {
    return view('auth.user-verified');
});

Route::post('/register-back', 'Auth\RegisterController@registerBack')->name('register-back');

Route::fallback(function () {
    return abort(404);
});

/**
 * Admin routing
 */
Route::group(
    [
        'prefix'        => 'admin',
        'middleware'    => 'admin'
    ],

    function () {
    Route::get('/', 'DashboardController@index');
    Route::get('/post/search', 'DashboardController@search')->name('search');
    Route::post('/post/delete/{post}', 'DashboardController@destroy');
    Route::post('/post/delete-image/{post}', 'DashboardController@destroyImage');
    Route::post('/post/delete-checked', 'DashboardController@destroyChecked');
    Route::post('/post/recover', 'DashboardController@recover');
});