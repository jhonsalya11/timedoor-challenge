<html>
    <head>
        <title>Timedoor Challenge - Level 8 | Register Success</title>

        <!-- CSS -->
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('user/css/bootstrap.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('user/css/style.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('user/css/tmdrPreset.css') }}">
        <!-- CSS End -->

        <!-- Javascript -->
        <script type="text/javascript" src="{{ URL::asset('user/js/jquery.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('user/js/bootstrap.min.js') }}"></script>
        <!-- Javascript End -->
    </head>

    <body id="login">
        <div class="box login-box text-center">
            <div class="login-box-head">
                <h1>Successfully Verified</h1>
            </div>
            <div class="login-box-body">
                <p>Thank you for your membership register.<br/>
                    Membership is now complete.</p>
            </div>
            <div class="login-box-footer">
                <div class="text-center">
                    <a href="/" class="btn btn-primary">Back to Home</a>
                </div>
            </div>
        </div>
    </body>

</html>