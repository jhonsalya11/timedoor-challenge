<html>
    <head>
        <title>Timedoor Challenge - Level 8</title>
        <!-- CSS -->
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('user/css/bootstrap.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('user/css/font-awesome.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('user/css/style.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('user/css/tmdrPreset.css') }}">
        <!-- CSS End -->

        <!-- Javascript -->
        <script type="text/javascript" src="{{ URL::asset('user/js/jquery.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('user/js/bootstrap.min.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('user/js/main.js') }}"></script>
        <!-- Javascript End -->

        @if(Session::has('modal'))
            @if (session('modal') === "editModal")
                <script>modal('editModal');</script>
            @else
                <script>modal('deleteModal');</script>
            @endif
        @endif
        @php
            $data = Session::get('data');
        @endphp
    </head>

    <body class="bg-lgray">
        <header>
        @include('user.layout.header')
        </header>
        <main>
            <div class="section">
                <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3 bg-white p-30 box">
                        <div class="text-center">
                            <h1 class="text-green mb-30"><b>Level 8 Challenge</b></h1>
                        </div>
                            @yield('form')
                        <hr>
                            @yield('content')
                        <div class="text-center mt-30">
                            @yield('pagination')
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </main>

        <footer>
            @include('user.layout.footer')
        </footer>

        {{-- Modal --}}
        @if ($data)
            @include('user.post.edit')
            @include('user.post.delete')
        @endif
    </body>
</html>