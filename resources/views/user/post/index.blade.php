@extends('user.layout.main')

@section('form')
    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif
    @php
        $user = Auth::user();
    @endphp
    <form method="POST" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <label>Name</label>
            @auth
                <input type="text" class="form-control" 
                    id="name" name="name" value="{{ $errors->hasBag('index') ? old('name') : $user->name }}">
            @else
                <input type="text" class="form-control" 
                    id="name" name="name" value="{{ $errors->hasBag('index') ? old('name') : '' }}">
            @endauth
            <p class="small text-danger mt-5">{{ $errors->index->first('name') }}</p>
        </div>
        <div class="form-group">
            <label>Title</label>
            <input type="text" class="form-control" 
                id="title" name="title" value="{{ $errors->hasBag('index') ? old('title') : '' }}">
            <p class="small text-danger mt-5">{{ $errors->index->first('title') }}</p>
        </div>
        <div class="form-group">
            <label>Body</label>
            <textarea rows="5" class="form-control" 
                id="body" name="body">{{ $errors->hasBag('index') ? old('body') : '' }}</textarea>
            <p class="small text-danger mt-5">{{ $errors->index->first('body') }}</p>
        </div>
        <div class="form-group">
            <label>Choose image from your computer :</label>
            <div class="input-group">
                <input type="text" class="form-control upload-form" value="No file chosen" readonly>
                <span class="input-group-btn">
                    <span class="btn btn-default btn-file">
                        <i class="fa fa-folder-open"></i>&nbsp;Browse <input id="image" name="image" type="file" multiple>
                    </span>
                </span>
            </div>
            <p class="small text-danger mt-5">{{ $errors->index->first('image') }}</p>
        </div>
        @guest
            <div class="form-group">
                <label>Password</label>
                <input type="password" class="form-control" id="password" name="password" 
                    value="{{ $errors->hasBag('index') ? old('password') : '' }}">
                <p class="small text-danger mt-5">{{ $errors->index->first('password') }}</p>
            </div>
        @endguest
        <div class="text-center mt-30 mb-30">
            <button type="submit" formaction="{{ route('store') }}" class="btn btn-primary">Submit</button>
        </div>
    </form>
@endsection

@section('content')
    @foreach ($posts as $post)
        @if (!$post->deleted_at)
            <div class="post">
                <div class="clearfix">
                    <div class="pull-left">
                        <h2 class="mb-5 text-green">
                            <b>{{ $post->title }}</b>
                        </h2>
                    </div>
                    <div class="pull-right text-right">
                        <p class="text-lgray">
                            {{ $post->created_at->format('d-m-Y') }}
                            <br/>
                            <span class="small">
                                {{ $post->created_at->format('H:i') }}
                            </span>
                        </p>
                    </div>
                </div>
                <h4 class="mb-20">
                    {{ $post->name }}
                    @if ($post->user)
                        <span class="text-id">[ ID : {{ $post->user->id }} ]</span>
                    @endif
                </h4>
                <p>{!! nl2br(e($post->body)) !!}</p>
                <div class="img-box my-10">
                    <img class="img-responsive img-post"
                    src="{{ $post->imageURL() }}"
                    alt="image">
                </div>
                <form method="POST" class="form-inline mt-50">
                    @csrf
                    @auth
                        @if ($post->user_id === $user->id)
                            <button type="submit" formaction="{{ route('edit', $post->id) }}" class="btn btn-default mb-2" data-toggle="modal">
                                <i class="fa fa-pencil p-3"></i>
                            </button>
                            <button type="submit" formaction="{{ route('delete', $post->id) }}" class="btn btn-danger mb-2" data-toggle="modal">
                                <i class="fa fa-trash p-3"></i>
                            </button>
                        @endif
                    @else
                        @if (!$post->user_id)
                            <div class="form-group mx-sm-3 mb-2">
                                <label for="inputPassword2" class="sr-only">Password</label>
                                <input type="password" class="form-control" name="inputPassword2" placeholder="Password">
                            </div>
                            <button type="submit" formaction="{{ route('edit', $post->id) }}" class="btn btn-default mb-2" data-toggle="modal">
                                <i class="fa fa-pencil p-3"></i>
                            </button>
                            <button type="submit" formaction="{{ route('delete', $post->id) }}" class="btn btn-danger mb-2" data-toggle="modal">
                                <i class="fa fa-trash p-3"></i>
                            </button>
                        @endif
                    @endauth
                </form>
            </div>
        @endif
    @endforeach
@endsection

@section('pagination')
    {{ $posts->onEachSide(5)->links() }}
@endsection