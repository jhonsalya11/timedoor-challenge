<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                @if(is_null(session('check')))
                    <h4 class="modal-title" id="myModalLabel">Edit Post</h4>
                @else
                    <p class="message-error">{{ session('check.messageNoPassword') }} {{ session('check.messageNotMatch') }}</p>
                @endif
            </div>
            <form method="POST" enctype="multipart/form-data">
                @csrf
                @if (!is_null(session('check.messageNotMatch')) || !is_null(session('check.messageNoPassword')))
                    <div class="modal-body">
                        <div class="clearfix">
                            <div class="pull-left">
                                <h2 class="mb-5 text-green">
                                    <b>{{ $data->title }}</b>
                                </h2>
                                <p>{{ $data->body }}</p>
                            </div>
                        </div>
                        <div class="img-box my-10">
                            <img class="img-responsive img-post"
                            src="{{ $data->imageURL() }}"
                            alt="image">
                        </div>
                        <div class="text-right">
                            <p class="text-lgray">
                                {{ $data->created_at->format('d-m-Y H:i') }}
                            </p>
                        </div>
                    </div>
                    <div class="modal-footer">
                        @if(!is_null(session('check.messageNotMatch')))
                            <form method="POST" class="form-inline">
                                @csrf
                                <div class="form-group">
                                    <label for="inputPassword2" class="sr-only">Password</label>
                                    <input type="password" class="form-control" id="inputPassword2" name="inputPassword2" placeholder="Password">
                                </div>
                                <button type="submit" formaction="{{ route('edit', $data->id) }}" class="btn btn-default mb-2" data-toggle="modal">
                                    Re-Enter Password
                                </button>
                            </form>
                        @else
                            <button type="button" class="btn btn-default" data-dismiss="modal">
                                Back to Page
                            </button>
                        @endif
                    </div>
                @else
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Name</label>
                            <input type="text" class="form-control" id="name" name="name" 
                                value="{{ old('name', $data->name) }}">
                            <p class="small text-danger mt-5">{{ $errors->modal->first('name') }}</p>
                        </div>
                        <div class="form-group">
                            <label>Title</label>
                            <input type="text" class="form-control" id="title" name="title" 
                                value="{{ old('title', $data->title) }}">
                            <p class="small text-danger mt-5">{{ $errors->modal->first('title') }}</p>
                        </div>
                        <div class="form-group">
                            <label>Body</label>
                            <textarea rows="5" class="form-control" id="body" name="body">{{ old('body', $data->body) }}</textarea>
                            <p class="small text-danger mt-5">{{ $errors->modal->first('body') }}</p>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-4">
                                <img class="img-responsive" alt="" src="{{ $data->imageURL() }}"> 
                            </div>
                            <div class="col-md-8 pl-0">
                                <label>Choose image from your computer :</label>
                                <div class="input-group">
                                    <input type="text" class="form-control upload-form" value="No file chosen" readonly>
                                    <span class="input-group-btn">
                                        <span class="btn btn-default btn-file">
                                            <i class="fa fa-folder-open"></i>&nbsp;Browse <input type="file" name="image" multiple>
                                        </span>
                                    </span>
                                </div>
                                <p class="small text-danger mt-5">{{ $errors->modal->first('image') }}</p>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="deleteImage">Delete image
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input name="password" type="hidden" value="{{ session('data.input_password') }}">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" formaction="{{ route('update', $data->id) }}" class="btn btn-primary">Save changes</button>
                    </div>
                @endif
            </form>
        </div>
    </div>
</div>