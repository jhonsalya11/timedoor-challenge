<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                @if (is_null(session('check')))
                    <h4 class="modal-title" id="myModalLabel">Delete Post</h4>
                @else
                    <p class="message-error">{{ session('check.messageNoPassword') }} {{ session('check.messageNotMatch') }}</p>
                @endif
            </div>
            <div class="modal-body pad-20">
                <div class="clearfix">
                    <div class="pull-left">
                        <h2 class="mb-5 text-green">
                            <b>{{ $data->title }}</b>
                        </h2>
                        <p>{!! nl2br(e($data->body)) !!}</p>
                    </div>
                </div>
                <div class="text-right">
                    <p class="text-lgray">
                        {{ $data->created_at->format('d-m-Y H:i') }}
                    </p>
                </div>
            </div>
            <div class="modal-footer">
                <form method="POST" class="form-inline">
                @csrf
                    @if (!is_null(session('check.messageNotMatch')))
                        <div class="form-group">
                            <label for="inputPassword2" class="sr-only">Password</label>
                            <input type="password" class="form-control" id="inputPassword2" name="inputPassword2" placeholder="Password">
                        </div>
                        <button type="submit" formaction="{{ route('delete', $data->id) }}" class="btn btn-default mb-2" data-toggle="modal">
                            Re-Enter Password
                        </button>
                    @elseif (!is_null(session('check.messageNoPassword')))
                        <button type="button" class="btn btn-default" data-dismiss="modal">
                            Back to Page
                        </button>
                    @else
                        <input name="currentPage" type="hidden" value="{{ $posts->currentPage() }}">
                        <input name="password" type="hidden" value="{{ session('data.input_password') }}">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button formaction="{{ route('destroy', $data->id) }}" type="submit" class="btn btn-danger">Delete</button>
                    @endif
                </form>
            </div>
        </div>
    </div>
</div>