<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;

use App\Models\Post;

class UpdatePost extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'      => 'nullable|min:3|max:16',
            'title'     => 'required|min:10|max:32',
            'body'      => 'required|min:10|max:200',
            'image'     => 'nullable|image|mimes:jpeg,jpg,png,gif|max:1024'
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $post = Post::findOrFail($this->get('id'));

        $post->input_password = $this->password;

        return redirect()
            ->back()
            ->with([
                'modal'   => 'editModal',
                'data'    => $post
            ])
            ->withErrors($validator, 'modal');
    }
}
