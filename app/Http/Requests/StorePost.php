<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Support\MessageBag;

class StorePost extends FormRequest
{
    protected $errorBag = 'index';

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'      => 'nullable|min:3|max:16',
            'title'     => 'required|min:10|max:32',
            'body'      => 'required|min:10|max:200',
            'image'     => 'nullable|image|mimes:jpeg,jpg,png,gif|max:1024',
            'password'  => 'numeric|nullable|digits:4'
        ];
    }
}
