<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\User;
use App\Http\Requests\StorePost;
use App\Http\Requests\UpdatePost;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

use Image;
use File;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // session()->flush();
        // paginate the view, 10 message per page
        $posts = Post::orderBy('id', 'desc')->paginate(10);
        return view('user.post.index', compact('posts'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePost $request)
    {
        $input = $request->all();

        $user = Auth::user();

        // move the image in the folder
        if ($originalImage = $request->file('image')) {
            $input['image'] = $this->saveImage($originalImage);
        }

        // create new data
        if($user){
            $user->posts()->create($input);
        } else {
            Post::create($input);
        }
        return redirect('/')->with($this->successMessage('added'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Post $post)
    {
        // compare the inputted password with the post password in database
        $check = $this->checkPassword($post->password, $request->inputPassword2, 'edit');
        if (Auth::guest()) {
            $post->input_password = $request->inputPassword2;
        }

        return redirect()
            ->back()
            ->with(
                $this->redirectModal('editModal', $post, $check)
            );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePost $request, Post $post)
    {
        $input = $request->except('password');
        
        $check = $this->checkPassword($post->password, $request->password);

        $this->checkUserAuth($check, $post);

        if ($request->deleteImage) {
            $post->deleteImage();

            $input['image'] = NULL;
        } else if ($request->image) {
            // if image selected and delete image is not checked
            if ($originalImage = $request->file('image')) {
                // set new imageName in post after saving the image
                $input['image'] = $this->saveImage($originalImage);
                $post->deleteImage();
            }
        }
        // Update the data
        $post->update($input);

        return redirect()->back()->with($this->successMessage('updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Post $post)
    {
        $check = $this->checkPassword($post->password, $request->password);

        $this->checkUserAuth($check, $post);

        Post::destroy($post->id);

        // redirect to the specific page
        $result = Post::paginate(10);
        $lastPage = $result->lastPage();
        // if the post is the last post for that page
        if ($request->currentPage > $lastPage) {
            // back to the previous page
            $url = '/?page=' . $lastPage;
            return redirect($url)->with($this->successMessage('deleted'));
        } else {
            return redirect()->back()->with($this->successMessage('deleted'));
        }
    }

    /**
     * Show the form for deleting specified resource.
     */
    public function delete(Request $request, Post $post)
    {
        $check = $this->checkPassword($post->password, $request->inputPassword2, 'delete');
        if (Auth::guest()) {
            $post->input_password = $request->inputPassword2;
        }

        return redirect()
            ->back()
            ->with(
                $this->redirectModal('deleteModal', $post, $check)
            );
    }

    /**
     * Check the password input compare it with the database
     */
    public function checkPassword($postPassword, $inputPassword, $modalName='')
    {
        if ( $postPassword ) {
            if (!Hash::check($inputPassword, $postPassword)) {
                // the password doesn't match
                return [
                    'messageNotMatch' => "The passwords you entered do not match. Please try again."
                ];
            }
        } else {
            if (Auth::guest()) {
                // the post doesn't have password
                return [
                    'messageNoPassword' => "This message can’t " . $modalName . ", because this message has not been set password."
                ];
            }
        }
    }

    /**
     * Save image to the storage and return the image name
     */
    public function saveImage($originalImage)
    {
        $name = time() . $originalImage->getClientOriginalName();
        $dataImage = Image::make($originalImage);

        if ( ! \File::isDirectory($originalPath = public_path() . Post::$originalPath) ) {
            \File::makeDirectory($originalPath, 493, true);
        }

        if ( ! \File::isDirectory($thumbnailPath = public_path() . Post::$thumbnailPath) ) {
            \File::makeDirectory($thumbnailPath, 493, true);
        }

        $dataImage->save($originalPath . $name);
        $dataImage->resize(300, null, function ($constraint) {
            $constraint->aspectRatio();
        });
        $dataImage->save($thumbnailPath . $name);

        return $name;
    }

    /**
     * Function for set session data for modal when redirecting
     */
    public function redirectModal($modal, $post, $check)
    {
        // new refactor redirect
        return [
            'modal'   => $modal,
            'data'    => $post,
            'check'   => $check
        ];
    }

    /**
     * Function for set success message to user
     */
    public function successMessage($action)
    {
        return [
            "success" => "Post  " . $action . "  successfully"
        ];
    }

    public function checkUserAuth($check, $post)
    {
        if ( Auth::guest() && !is_null($check) || Auth::user() && !$post->user_id == Auth::id() ) {
            return abort(403, 'Unauthorized action.');
        }
    }
}
