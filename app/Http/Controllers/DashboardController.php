<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {
        $posts = Post::withTrashed()->orderBy('id', 'desc')->paginate(10);

        return view('admin.post.index', compact('posts'));
    }

    public function search(Request $request)
    {
        $params = $request->except('_token');

        $query = Post::withTrashed()->where([
            ['title', 'LIKE', "%{$request->title}%"],
            ['body', 'LIKE', "%{$request->body}%"]
        ]);

        if ($request->imageOption === 'with') {
            $query->whereNotNull('image');
        } else if ($request->imageOption === 'without') {
            $query->whereNull('image');
        }

        if ($request->statusOption === 'on') {
            $query->whereNull('deleted_at');
        } else if ($request->statusOption === 'delete') {
            $query->whereNotNull('deleted_at');
        }

        $posts = $query->orderBy('id', 'desc')->paginate(10);

        $posts->appends($params);

        return view('admin.post.index', compact('posts', 'params'));
    }

    public function destroy(Request $request, Post $post)
    {
        $post->delete();

        return redirect()->back();
    }

    public function destroyImage(Request $request, Post $post)
    {
        $input['image'] = NULL;
        
        $post->update($input);

        $post->deleteImage();

        return redirect()->back();
    }

    public function destroyChecked(Request $request)
    {
        $postIDArray = explode(",", $request->id_delete);

        Post::destroy($postIDArray);

        return redirect()->back();
    }

    public function recover(Request $request)
    {
        Post::onlyTrashed()->where('id',$request->id)->restore();

        return redirect()->back();
    }
}
