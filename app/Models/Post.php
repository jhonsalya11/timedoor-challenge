<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
    const IMAGE_PATH = '/storage/images/posts';

    use SoftDeletes;

    protected $fillable = [
        'name', 
        'title', 
        'body', 
        'image',
        'password'
    ];

    public static $thumbnailPath = self::IMAGE_PATH . '/thumbnail/';
    public static $originalPath = self::IMAGE_PATH . '/original/';

    public function imageURL($type = 'thumbnail')
    {
        if ($this->image) {
            return self::IMAGE_PATH . '/' . $type . '/' . $this->image;
        } else {
            return 'http://via.placeholder.com/500x500';
        }
    }

    public function setPasswordAttribute($password)
    {
        if (!empty($password)) {
            $this->attributes['password'] = Hash::make($password);
        }
    }

    public function getNameAttribute($value)
    {
        if ($value) {
            return $value;
        } else {
            return 'No Name';
        }
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    /**
     * Delete image from the database and storage
     */
    public function deleteImage()
    {
        if ($this->image) {
            // unlink image in original and also thumbnail path
            if (file_exists($thumbnailImage = public_path() . Post::$thumbnailPath . $this->image)) {
                unlink($thumbnailImage);
            }
            if (file_exists($originalImage = public_path() . Post::$originalPath . $this->image)) {
                unlink($originalImage);
            }
        }
    }

    /**
     * Perform the actual delete query on this model instance.
     * This is an override method
     * @return void
     */
    protected function runSoftDelete()
    {
        $query = $this->setKeysForSaveQuery($this->newModelQuery());

        $time = $this->freshTimestamp();

        $columns = [$this->getDeletedAtColumn() => $this->fromDateTime($time)];

        $this->{$this->getDeletedAtColumn()} = $time;

        if ($this->timestamps && ! is_null($this->getUpdatedAtColumn())) {
            $this->{$this->getUpdatedAtColumn()} = $time;

            $columns[$this->getUpdatedAtColumn()] = $this->fromDateTime($time);
            $columns['image'] = NULL;
        }

        $query->update($columns);
        $this->deleteImage();
    }
}
